require 'faker'
FactoryBot.define do
    factory :profile do
      first_name { Faker::Lorem.word }
      last_name { Faker::Lorem.word }
      username { Faker::Lorem.word }
      email { Faker::Lorem.word }
    end
  end