require 'rails_helper'

RSpec.describe ProfilesController, type: :controller do

  describe '#api_testing' do

    describe 'GET #index when profile object is present' do
      let!(:profiles) { create_list(:profile, 2) }
      before(:each) do
        get :index
      end

      let(:json_response) { JSON.parse(response.body) }

      it 'should respond with status 200' do
        expect(response.status).to be 200
      end

      it 'should respond with keys profiles' do
        expect(json_response.count).to eq(profiles.count)
      end
    end

    describe '#destroy when profile is deleted' do
      let!(:profile) { create(:profile ) }
      before(:each) do
        delete :destroy, params: {id: profile.id}
      end
      let(:json_response) { JSON.parse(response.body) }

      it 'should respond with status 200' do
        expect(response.status).to be 200
      end

      context 'when profile is deleted success case for keys and values' do
        it 'should have keys success and message' do
          expect(json_response.keys).to match_array(["status", "message"])
        end

        it 'should respond with status success' do
          expect(json_response["status"]).to eq("success")
        end

        it 'should respond with profile delete message' do
          expect(json_response["message"]).to eq("Profile Successfully Deleted")
        end
      end
    end

    describe '#create when profile is not created' do
      before(:each) do
        post :create, { params: { first_name: "", last_name: "",username: "",email: "" } }
      end

      let(:json_response) { JSON.parse(response.body) }

      it 'should respond with status 200' do
        expect(response.status).to be 200
      end

      context 'it should respond with keys and values' do
        it 'should respond with keys' do
          expect(json_response.keys).to eq(["status","errors"])
        end

        it 'should respond with status error' do
          expect(json_response["status"]).to eq("error")
        end

        it 'should respond with errors' do
          expect(json_response["errors"]).to eq( ["First name can't be blank", "Last name can't be blank", "Email can't be blank", "Username can't be blank"]  )
        end

      end
    end

    describe '#create when profile is  created' do
      before(:each) do
        post :create, { params: { first_name: "dsad", last_name: "dsad",username: "dsada",email: "dsadad" } }
      end

      let(:json_response) { JSON.parse(response.body) }

      it 'should respond with status 200' do
        expect(response.status).to be 200
      end

      context 'it should respond with keys and values' do
        it 'should respond with keys' do
          expect(json_response.keys).to eq(["status"])
        end

        it 'should respond with status error' do
          expect(json_response["status"]).to eq("success")
        end
      end
    end

    describe '#update when profile is not updated' do
      let!(:profile) { create(:profile ) }
      before(:each) do
        put :update, { params: { id: profile.id, first_name: "", last_name: "",username: "",email: "" } }
      end

      let(:json_response) { JSON.parse(response.body) }

      it 'should respond with status 200' do
        expect(response.status).to be 200
      end

      context 'it should respond with keys and values' do
        it 'should respond with keys' do
          expect(json_response.keys).to eq(["status","errors"])
        end

        it 'should respond with status error' do
          expect(json_response["status"]).to eq("error")
        end

        it 'should respond with errors' do
          expect(json_response["errors"]).to eq( ["First name can't be blank", "Last name can't be blank", "Email can't be blank", "Username can't be blank"]  )
        end
      end
    end

    describe '#update when profile is not updated' do
      let!(:profile) { create(:profile ) }
      before(:each) do
        put :update, { params: { id: profile.id, first_name: "dsd", last_name: "dsd",username: "dsd",email: "dsd" } }
      end

      let(:json_response) { JSON.parse(response.body) }

      it 'should respond with status 200' do
        expect(response.status).to be 200
      end
      context 'it should respond with keys and values' do
        it 'should respond with keys' do
          expect(json_response.keys).to eq(["status","profile"])
        end
        it 'should respond with status' do
          expect(json_response["status"]).to eq("success")
        end

        it 'should respond with id' do
          expect(json_response["profile"]["id"]).to eq(profile.id)
        end

        it 'should respond with first_name' do
          expect(json_response["profile"]["first_name"]).to eq("dsd")
        end

        it 'should respond with last_name' do
          expect(json_response["profile"]["last_name"]).to eq("dsd")
        end

        it 'should respond with email' do
          expect(json_response["profile"]["email"]).to eq("dsd")
        end

      end
    end

    describe '#update when profile is not updated' do
      let!(:profile) { create(:profile ) }
      before(:each) do
        get :show, { params: { id: profile.id} }
      end

      let(:json_response) { JSON.parse(response.body) }

      it 'should respond with status 200' do
        expect(response.status).to be 200
      end
      context 'it should respond with keys and values' do
        it 'should respond with keys' do
          expect(json_response.keys).to eq(["status","profile"])
        end
        it 'should respond with status' do
          expect(json_response["status"]).to eq("success")
        end

        it 'should respond with id' do
          expect(json_response["profile"]["id"]).to eq(profile.id)
        end

        it 'should respond with first_name' do
          expect(json_response["profile"]["first_name"]).to eq(profile.first_name)
        end

        it 'should respond with last_name' do
          expect(json_response["profile"]["last_name"]).to eq(profile.last_name)
        end

        it 'should respond with email' do
          expect(json_response["profile"]["email"]).to eq(profile.email)
        end
      end
    end
  end
end
