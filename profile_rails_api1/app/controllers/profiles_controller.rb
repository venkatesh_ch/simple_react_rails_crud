class ProfilesController < ApplicationController
    before_action :find_profile, only: [:show, :update, :destroy]
    def index
        @profiles = Profile.all
        render json: @profiles, status: :ok
    end
    def create
        @profile = Profile.new(profile_params)
        if @profile.save
            render json: { status: "success" }
        else
            puts "inside error"
            render json: { status: "error", errors: @profile.errors.full_messages }
        end
    end

    def update
        if @profile.update_attributes(profile_params)
            render json: { status: "success" , profile: @profile}
        else
            render json: { status: "error", errors: @profile.errors.full_messages }
        end
    end

    def show
        render json: { status: "success", profile: @profile}
    end

    def destroy
        if @profile.destroy
            render json: { status: "success" , message: "Profile Successfully Deleted"}
        else
            render json: { status: "error", errors: @profile.errors.full_messages }
        end
    end

    def find_profile
        @profile = Profile.find(params[:id])
    end


    def profile_params
        params.permit(:first_name, :last_name, :username, :gender, :email, :date_of_birth)
    end
end
