class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :username
      t.integer :age
      t.string :gender, :limit => 1
      t.string :email, :limit => 60
      t.datetime :date_of_birth
      t.timestamps
    end
  end
end
